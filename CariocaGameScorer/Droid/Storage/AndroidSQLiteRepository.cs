﻿using System;
using System.IO;
using CariocaGameScorer.Storage.Interfaces;
using CariocaGameScorer.Android.Storage;
using SQLite.Net.Platform.XamarinAndroid;
using SQLite.Net;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidSQLiteRepository))]
namespace CariocaGameScorer.Android.Storage
{
	public class AndroidSQLiteRepository : ISqliteRepository
	{
		public SQLiteConnection GetConnection()
		{
			string dbPath = Path.Combine(
				System.Environment.GetFolderPath(Environment.SpecialFolder.Personal),
				"CasiocaScorer.Android.db3");
			return new SQLiteConnection(new SQLitePlatformAndroid(), dbPath); ;
		}
	}
}
