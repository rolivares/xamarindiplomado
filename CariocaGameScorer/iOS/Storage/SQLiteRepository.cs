﻿using System;
using System.IO;
using CariocaGameScorer.Storage.Interfaces;
using CariocaGameScorer.iOS.Storage;
using SQLite.Net.Platform.XamarinIOS;
using SQLite.Net;

[assembly: Xamarin.Forms.Dependency(typeof(SQLiteRepository))]
namespace CariocaGameScorer.iOS.Storage
{
	public class SQLiteRepository : ISqliteRepository
	{
		public SQLiteConnection GetConnection()
		{
			string dbPath = Path.Combine(
				System.Environment.GetFolderPath(Environment.SpecialFolder.Personal),
				"CasiocaScorer.iOS.db3");
			return new SQLiteConnection(new SQLitePlatformIOS(), dbPath); ;
		}
	}
}
