using System;
using CariocaGameScorer.Storage.Interfaces;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace CariocaGameScorer.Model
{
	public class ScoreByPlayer : IKeyObject
	{
		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }

		[OneToOne]
		public Player Player { get; set; }

		[ForeignKey(typeof(Player)), NotNull]
		public int PlayerId { get; set; }

		[ForeignKey(typeof(Hand))]
		public int HandId { get; set; }


		public int? Score { get; set; }

		public override string ToString()
		{
			return string.Format("[Player={1}, Score={3}]", Id, "", HandId, Score);
		}

	}
}
