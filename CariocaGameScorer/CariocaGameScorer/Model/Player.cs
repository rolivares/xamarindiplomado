using System;
using System.Collections.Generic;
using CariocaGameScorer.Storage.Interfaces;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace CariocaGameScorer.Model
{
	public class Player : IKeyObject
	{
		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }

		[ForeignKey(typeof(Game))]
		public int GameId { get; set; }

		[ManyToOne]
		public Game Game { get; set; }

		public string Fullname { get; set; }
		public string Nickname { get; set; }


		public override string ToString()
		{
			return string.Format("[ Id={0}, Fullname={1}]", Id, Fullname, Nickname);
		}
	}
}
