using System;
using System.Collections.Generic;
using CariocaGameScorer.Storage.Interfaces;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace CariocaGameScorer.Model
{
	/// <summary>
	/// Common people to play with
	/// </summary>
	public class Room : IKeyObject
	{
		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }

		public string Title { get; set; }

		/// <summary>
		/// Games created
		/// </summary>
		[OneToMany]
		public List<Game> Games { get; set; }

		[OneToMany]
		public List<Player> Players { get; set; }

		public override string ToString()
		{
			return string.Format("[Room: Id={0}, Title={1}, Games={2}, Players={3}]", Id, Title, Games, Players);
		}

	}
}
