using System;
using System.Collections.Generic;
using CariocaGameScorer.Storage.Interfaces;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace CariocaGameScorer.Model
{
	public enum HandStatus
	{
		Pending = 1,
		Playing = 2,
		Played = 4,
		Skipped = 8
	}


	public class Hand : IKeyObject
	{

		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }
		public int Total { get; set; }
		public string Title { get; set; }
		public int PlayOrder { get; set; }
		public HandStatus Status { get; set; }

		[ForeignKey(typeof(Game))]
		public int GameId { get; set; }

		//[ManyToOne]
		//public Game Game { get; set; }

		[OneToMany(CascadeOperations = CascadeOperation.All)]
		public List<ScoreByPlayer> Scores { get; set; }

		public Hand()
		{
			this.Scores = new List<ScoreByPlayer>();
		}

		public override string ToString()
		{
			return string.Format("[Id={0}, Title={2}, Status={4}]", Id, Total, Title, PlayOrder, Status, GameId, "", Scores);
		}

	}
}
