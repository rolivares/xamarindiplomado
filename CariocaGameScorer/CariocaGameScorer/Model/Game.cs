using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;
using CariocaGameScorer.Storage.Interfaces;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace CariocaGameScorer.Model
{
	public enum GameStatus
	{
		New,
		Started,
		Stopped,
		Ended
	}



	/// <summary>
	/// Current game that have a score associated
	/// </summary>
	public class Game : IKeyObject
	{

		[AutoIncrement, PrimaryKey]
		public int Id { get; set; }

		public DateTime CreatedOn { get; set; }
		public DateTime FinishedOn { get; set; }

		[NotNull]
		public string Title { get; set; }

		/// <summary>
		/// Set of hands to play on the game (2T, 1T1E .... ES, ER, EL
		/// </summary>
		[OneToMany(CascadeOperations = CascadeOperation.All)]
		public List<Hand> Hands { get; set; }

		/// <summary>
		/// Players associated with the current game, each hand is played for each player on game
		/// </summary>
		[OneToMany(CascadeOperations = CascadeOperation.All)]
		public List<Player> Players { get; set; }


		public Game()
		{
			this.Hands = new List<Hand>();
		}

		/// <summary>
		/// Calculate the leader board based on played hands
		/// </summary>
		/// <returns>The leader board.</returns>
		public List<ScoreByPlayer> GetLeaderBoard()
		{
			//var leaderBoard = HandsOnGame.Where(h => h.Status == HandStatus.Played).SelectMany(x => x.PlayerScores)
			//					   .GroupBy(x => x.Player)
			//					   .Select(g => new PlayerScore() { Player = g.Key, Score = g.Sum(s => s.Score ?? 0) })
			//					   .ToList();
			//return leaderBoard;
			return null;
		}

		public override string ToString()
		{
			return string.Format("[Title={3}, Players={5}]", Id, CreatedOn, FinishedOn, Title, Hands, Players);
		}
	}
}
