﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CariocaGameScorer.Model;
using CariocaGameScorer.Storage;
using System.Linq;

namespace CariocaGameScorer.Services
{
	public class GamesService
	{
		public IList<Game> LoadGames()
		{
			var dbManager = new DatabaseManager();
			var list = dbManager.GetAllItems<Game>();

			if (list.Count == 0)
			{
				list = DummyObjectsProvider.CreateGames(5, 8);
				dbManager.SaveValues(list);

				list = dbManager.GetAllItems<Game>();

			}

			return list;
		}

	}

}
