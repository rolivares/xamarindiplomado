﻿using System;
using System.Collections.Generic;
using CariocaGameScorer.Model;
using CariocaGameScorer.ViewModel;
using Xamarin.Forms;

namespace CariocaGameScorer.Views
{
	public partial class GamesPage : ContentPage
	{
		public GamesPage()
		{
			InitializeComponent();
			var viewModel = new GamesViewModel();
			this.BindingContext = viewModel;
			lvGames.ItemSelected += LvGames_ItemSelected;
			viewModel.LoadGames();

		}

		void LvGames_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (lvGames.SelectedItem == null)
				return;

			var selectedGame = e.SelectedItem as Game;

			if (selectedGame == null)
				return;

			Navigation.PushAsync(new Views.GameDetailsPage(selectedGame.Id), true);
			lvGames.SelectedItem = null;
		}
	}
}
