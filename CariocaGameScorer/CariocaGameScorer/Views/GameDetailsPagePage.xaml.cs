﻿using System;
using System.Collections.Generic;
using CariocaGameScorer.Model;
using Xamarin.Forms;

namespace CariocaGameScorer.Views
{
	public partial class GameDetailsPage : ContentPage
	{
		public GameDetailsPage(Game selectedGame)
		{
			InitializeComponent();
			this.BindingContext = selectedGame;
		}

		public GameDetailsPage(int gameId)
		{
			InitializeComponent();
			var selectedGame = new DatabaseManager().GetByKey<Game>(gameId);
			this.BindingContext = selectedGame;
			lvPlayersOnGame.BindingContext = selectedGame;
		}
	}
}
