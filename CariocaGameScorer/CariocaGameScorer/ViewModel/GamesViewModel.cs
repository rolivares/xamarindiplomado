﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using CariocaGameScorer.Model;

namespace CariocaGameScorer.ViewModel
{
	public class GamesViewModel : BaseObservableObject
	{
		private bool _isBusy;
		public bool IsBusy
		{
			get { return _isBusy; }
			set { _isBusy = value; OnPropertyChanged(); }
		}

		public ObservableCollection<Game> Games { get; set; }

		public ICommand LoadGamesList { get; private set; }

		public GamesViewModel()
		{
			this.Games = new ObservableCollection<Game>();
			this.LoadGamesList = new Xamarin.Forms.Command(LoadGames);
		}

		public void LoadGames()
		{
			this.IsBusy = true;
			var service = new Services.GamesService();
			var rooms = service.LoadGames();
			foreach (var room in rooms)
				this.Games.Add(room);
			this.IsBusy = false;
		}

	}
}
