﻿using Xamarin.Forms;

namespace CariocaGameScorer
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			new DatabaseManager().CreateDb();
			var view = new Views.GamesPage();
			MainPage = new NavigationPage(view);
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}


	}
}
