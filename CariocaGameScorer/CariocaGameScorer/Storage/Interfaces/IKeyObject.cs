﻿using System;
namespace CariocaGameScorer.Storage.Interfaces
{
	public interface IKeyObject
	{
		int Id { get; set; }
	}
}
