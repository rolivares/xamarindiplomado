﻿using System;
using SQLite.Net;

namespace CariocaGameScorer.Storage.Interfaces
{
	public interface ISqliteRepository
	{
		SQLiteConnection GetConnection();

	}


}
