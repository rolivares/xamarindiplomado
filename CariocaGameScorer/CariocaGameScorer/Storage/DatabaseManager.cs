﻿using System;
using System.Collections.Generic;
using CariocaGameScorer.Storage.Interfaces;
using System.Linq;
using Xamarin.Forms;
using CariocaGameScorer.Model;
using SQLite.Net;
using SQLite.Net.Interop;
using SQLiteNetExtensions;
using SQLiteNetExtensions.Extensions;


namespace CariocaGameScorer
{
	public class DatabaseManager
	{
		private SQLiteConnection _connection;

		public DatabaseManager()
		{
			_connection = DependencyService.Get<ISqliteRepository>().GetConnection();

		}

		public void SaveValue<T>(T value, bool recursive = true) where T : IKeyObject, new()
		{
			if (value.Id > 0)
				_connection.UpdateWithChildren(value);
			else
				_connection.InsertWithChildren(value, recursive: recursive);
		}

		public void SaveValues<T>(IList<T> values) where T : IKeyObject, new()
		{
			foreach (var item in values)
				SaveValue(item);
		}

		public T GetByKey<T>(int key) where T : class, IKeyObject, new()
		{
			return _connection.GetWithChildren<T>(key, recursive: true);
		}


		public IList<T> GetAllItems<T>() where T : class, IKeyObject, new()
		{
			return _connection.GetAllWithChildren<T>(recursive: true).ToList();
		}

		public void CreateDb()
		{


			_connection.DropTable<Player>();
			_connection.DropTable<ScoreByPlayer>();
			_connection.DropTable<Hand>();
			//_connection.DropTable<HandScore>();
			_connection.DropTable<Game>();

			_connection.CreateTable<Player>(CreateFlags.None);
			_connection.CreateTable<ScoreByPlayer>(CreateFlags.None);
			_connection.CreateTable<Hand>(CreateFlags.None);
			//_connection.CreateTable<HandScore>(CreateFlags.None);
			_connection.CreateTable<Game>(CreateFlags.None);

		}
	}
}
