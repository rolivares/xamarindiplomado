﻿using System;
using System.Collections.Generic;
using CariocaGameScorer.Model;

namespace CariocaGameScorer.Storage
{
	public static class DummyObjectsProvider
	{
		private static IList<string> _names = new List<string>() { "Sobaco", "Asesino", "Capitán", "Pezón", "Trueno", "Lobo", "Conejo", "Halcón", "Sargento", "Prepucio", "El milagro", "El rey", "Choro" };
		private static IList<string> _lastNames = new List<string>() { "Elástico", "Carmesí", "Radioactivo", "Volador", "Espacial", "Letal", "Flácido", "Marciano", "Oscuro", "Invisible", "Mutante", "Vengador", "Amoroso" };



		public static IList<Game> CreateGames(
				int rooms = 3,
				int playersMax = 6)
		{
			var gamesList = new List<Game>();
			var rnd = new Random(DateTime.Now.Second);
			for (int i = 0; i < rooms; i++)
			{
				var date = DateTime.Now.AddDays(-(rnd.Next(30, 365)));
				var game = new Game()
				{
					Title = "Playa " + date.ToString("yyyy-MMMM-dd"),
					Players = CreatePlayers(rnd.Next(2, playersMax)),
					CreatedOn = date
				};

				game.Hands = CreateHands(game.Players);

				gamesList.Add(game);
			}

			return gamesList;
		}


		private static List<Hand> CreateHands(IList<Player> players)
		{
			var commonHands = GetCommonHands();
			var rnd = new Random(DateTime.Now.Second);
			var winnerPlayer = 0;
			foreach (var hand in commonHands)
			{
				winnerPlayer = rnd.Next(0, players.Count - 1);
				for (int i = 0; i < players.Count; i++)
				{
					hand.Scores.Add(new ScoreByPlayer()
					{	
						Player = players[i],
						Score = rnd.Next(winnerPlayer == i ? 0 : 2, 100)
					});
				}
			}
			return commonHands;
		}

		public static List<Model.Player> CreatePlayers(int q)
		{
			var list = new List<Model.Player>();
			var rnd = new Random(DateTime.Now.Second);
			for (int i = 0; i < q; i++)
			{
				var name = _names[rnd.Next(0, _names.Count - 1)];
				list.Add(new Model.Player()
				{
					Fullname = name + " " + _lastNames[rnd.Next(0, _lastNames.Count - 1)],
					Nickname = name
				});
			}
			return list;
		}


		public static List<Hand> GetCommonHands()
		{
			var hands = new List<Hand>();
			hands.Add(new Hand() { Total = 6, Title = "2T" });
			hands.Add(new Hand() { Total = 7, Title = "1T 1E" });
			hands.Add(new Hand() { Total = 8, Title = "2E" });
			hands.Add(new Hand() { Total = 9, Title = "3T" });
			hands.Add(new Hand() { Total = 10, Title = "2T 1E" });
			hands.Add(new Hand() { Total = 11, Title = "1T 2E" });

			hands.Add(new Hand() { Total = 12, Title = "4T" });
			hands.Add(new Hand() { Total = 12, Title = "3E" });

			hands.Add(new Hand() { Total = 13, Title = "Escala loca" });
			hands.Add(new Hand() { Total = 13, Title = "Escala sucia" });
			hands.Add(new Hand() { Total = 13, Title = "Escala Real" });

			return hands;
		}
	}
}
