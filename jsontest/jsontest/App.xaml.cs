﻿using Xamarin.Forms;

namespace jsontest
{
	public partial class App : Application
	{
		private jsontestPage _mainPage;

		public App()
		{
			_mainPage = new jsontestPage();
			InitializeComponent();
			MainPage = _mainPage;
			//Thickness = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 0);
		}

		protected override void OnStart()
		{

		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
