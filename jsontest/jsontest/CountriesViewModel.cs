﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace jsontest
{
	public class CountriesViewModel : BaseObservableObject
	{
		public ObservableCollection<Country> Countries { get; set; }
		public RestClient _restClient = new RestClient();

		public Command LoadCountriesCommand { get; private set; }

		private bool _isBusy;
		public bool IsBusy
		{
			get { return _isBusy; }
			set { _isBusy = value; OnPropertyChanged(); }
		}

		public CountriesViewModel()
		{
			this.Countries = new ObservableCollection<Country>();
			this.LoadCountriesCommand = new Command((obj) => LoadCountries());
		}

		public async void LoadCountries()
		{
			this.IsBusy = true;
			var result = await _restClient.GetContriesAsync();
			Countries.Clear();
			foreach (var item in result)
			{
				Countries.Add(item);
			}
			this.IsBusy = false;
		}
	}
}
