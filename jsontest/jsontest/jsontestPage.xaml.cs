﻿using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace jsontest
{
	public partial class jsontestPage : ContentPage
	{
		public jsontestPage()
		{
			InitializeComponent();
			var vm = new CountriesViewModel();
			this.BindingContext = vm;

			vm.LoadCountries();
		}



	}
}
