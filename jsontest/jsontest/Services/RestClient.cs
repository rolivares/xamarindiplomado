﻿using System;
using System.Collections;
using Newtonsoft.Json;
using System.Net;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;

namespace jsontest
{
	public class RestClient
	{

		public static string Serialize(object value)
		{
			var ser = JsonConvert.SerializeObject(value);
			return ser;
		}

		public static T Deserialize<T>(string value)
		{
			return (T)JsonConvert.DeserializeObject<T>(value);
		}


		public async Task<IList<Country>> GetContriesAsync()
		{
			var list = await GetDeserializedList<Country>("https://restcountries.eu/rest/v1/all");
			return list.ToList();
		}


		public async Task<IEnumerable<T>> GetDeserializedList<T>(string url)
		{
			var result = Enumerable.Empty<T>();
			using (var httpClient = new HttpClient())
			{
				httpClient.DefaultRequestHeaders.Accept.Clear();
				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				//httpClient.BaseAddress = new Uri(url);
				var responseAsync = await httpClient.GetAsync(new Uri(url)).ConfigureAwait(false);

				if (responseAsync.IsSuccessStatusCode)
				{
					var json = await responseAsync.Content.ReadAsStringAsync().ConfigureAwait(false);

					result = await Task.Run(() =>
					{
						return JsonConvert.DeserializeObject<IEnumerable<T>>(json);
					}).ConfigureAwait(false);
				}
			}

			return result;
		}


	}
}
