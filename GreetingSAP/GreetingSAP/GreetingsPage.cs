﻿using System;
using Xamarin.Forms;

namespace GreetingSAP
{
	public class GreetingsPage : ContentPage
	{
		public GreetingsPage()
		{
			var myLabel = new Label() { 
				Text = "Greetings Xamarins Forms !!!",
				//HorizontalOptions = LayoutOptions.Center,
				//VerticalOptions = LayoutOptions.Center
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center
			};
			this.Content = myLabel;

			//#if __IOS__
			//			Padding = new Thickness(0, 20, 0, 0);
			//#endif

			//this.Padding = Device.OnPlatform<Thickness>(
			//	new Thickness(0, 20, 0, 0),
			//	new Thickness(0),
			//	new Thickness(0)
			//);

			Device.OnPlatform(iOS: () =>
			{
				Padding = new Thickness(0, 20, 0, 0);
			});
		}
	}
}

