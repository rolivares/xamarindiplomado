﻿using System;

using Xamarin.Forms;

namespace HelloXamarinFormsSAP_noXAML
{
	public class App : Application
	{
		public App()
		{
			var baseMessage = "Welcome to Xamarin Forms!";
#if __IOS__ 
			baseMessage += " desde iOs!!";
#elif __ANDROID__
			baseMessage += " desde android!!";

			#endif

			// The root page of your application
			var content = new ContentPage
			{
				Title = "HelloXamarinFormsSAP_noXAML",
				Content = new StackLayout
				{
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							HorizontalTextAlignment = TextAlignment.Center,
							Text = baseMessage
						}
					}
				}
			};

			MainPage = new NavigationPage(content);
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
