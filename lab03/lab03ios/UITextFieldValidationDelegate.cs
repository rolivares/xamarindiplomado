﻿using System;
using UIKit;

namespace lab03ios
{
	public class UITextFieldValidationDelegate : UITextFieldDelegate
	{
		private ValidationType _validationType;

		public UITextFieldValidationDelegate(ValidationType validationType) : base()
		{
			_validationType = validationType;
		}

		public override bool ShouldChangeCharacters(UITextField textField, Foundation.NSRange range, string replacementString)
		{
			return Validation.Validate(replacementString, _validationType);
		}
	}
}
