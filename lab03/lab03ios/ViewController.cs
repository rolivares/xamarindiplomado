﻿using System;

using UIKit;

namespace lab03ios
{
	public partial class ViewController : UIViewController
	{
		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			btnStart.TouchUpInside += BtnStart_TouchUpInside;
			txtCode.Delegate = new UITextFieldValidationDelegate(ValidationType.Code);
			txtUser.Delegate = new UITextFieldValidationDelegate(ValidationType.Username);
			txtPass.Delegate = new UITextFieldValidationDelegate(ValidationType.Password);
		}

		void BtnStart_TouchUpInside(object sender, EventArgs e)
		{
			txtCode.Enabled = (txtUser.HasText && txtPass.HasText);
			if (txtCode.Enabled)
			{
				btnStart.TouchUpInside -= BtnStart_TouchUpInside;
				btnStart.TouchUpInside += (senderBtn, eBtn) => {
					this.ShowPopup("desde el botón", "mi mensaje desde el botón");
				};
			}



		}

		partial void ValueChanged(UITextField sender)
		{
				txtPass.Enabled = sender.HasText;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
