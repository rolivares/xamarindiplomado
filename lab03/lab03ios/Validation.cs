﻿using System;
namespace lab03ios
{
	public enum ValidationType
	{
		Password,
		Username,
		Code
	}

	public class Validation
	{
		public static bool Validate(string text, ValidationType validationType)
		{
			if (string.IsNullOrEmpty(text))
				return true;
			
			switch (validationType)
			{
				case ValidationType.Code:
					return !text.ToLower().Contains("c");
				case ValidationType.Password:
					return !text.ToLower().Contains("p");
				case ValidationType.Username:
					return !text.ToLower().Contains("u");
			}
			return false;
		}
	}
}
