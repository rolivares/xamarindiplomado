﻿using System;
using UIKit;

namespace lab03ios
{
	public static class PopupFactory
	{
		public static void ShowPopup(this UIViewController ctrl, string title, string message)
		{

			var alert = new UIAlertController();
			alert.Title = title;
			alert.Message = message;
			alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, (obj) => { }));
			alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, (obj) => { }));
			alert.AddAction(UIAlertAction.Create("NOOOO!", UIAlertActionStyle.Destructive, (obj) => { }));
			
			ctrl.PresentViewController(alert, false, null);
		}
	}
}
