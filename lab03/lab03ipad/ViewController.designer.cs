// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace lab03ipad
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        MapKit.MKMapView mapa { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl selector { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView visor { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIWebView visorWeb { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (mapa != null) {
                mapa.Dispose ();
                mapa = null;
            }

            if (selector != null) {
                selector.Dispose ();
                selector = null;
            }

            if (visor != null) {
                visor.Dispose ();
                visor = null;
            }

            if (visorWeb != null) {
                visorWeb.Dispose ();
                visorWeb = null;
            }
        }
    }
}