﻿using System;
namespace lab03ipad
{
	public class Dato
	{
		public string Titulo { get; private set; }
		public double Lat { get; private set; }
		public double Lon { get; private set; }

		public Dato(string titulo, double x, double y) {
			this.Titulo = titulo;
			this.Lat = x;
			this.Lon = y;
		}
	}
}
