﻿using System;
using System.Collections.Generic;
using MapKit;
using UIKit;

namespace lab03ipad
{
	public partial class ViewController : UIViewController
	{
		private List<Dato> _lista = new List<Dato>() {
			new Dato("uno", 21, -101),
			new Dato("dos", 21,-86),
			new Dato("tres", 32,-117)
		};

		public IList<Dato> GetDatos()
		{
			return _lista;
		}

		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			visor.Image = UIImage.FromFile("img.jpg");
			visorWeb.LoadHtmlString("<html><body>hollaaaa!</body></html>", new Foundation.NSUrl("./", true));

			selector.ValueChanged += (sender, e) => { 
			  switch (selector.SelectedSegment)
				{
					case 0:
						mapa.MapType = MapKit.MKMapType.Standard;
						break;
					case 1:
						mapa.MapType = MapKit.MKMapType.Satellite;
						break;
					case 2:
						mapa.MapType = MapKit.MKMapType.Hybrid;
						break;
					default:
						break;
				}
			};

			_lista.ForEach(x => {
				mapa.AddAnnotation(new MKPointAnnotation() { 
				 Title =  x.Titulo,
					Coordinate = new CoreLocation.CLLocationCoordinate2D(x.Lat, x.Lon)
				});
			});
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
