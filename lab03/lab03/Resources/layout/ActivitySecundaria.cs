﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace lab03
{
	[Activity(Label = "ActivitySecundaria")]
	public class ActivitySecundaria : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.ActivitySecundaria);
			try
			{
				// Create your application here
				FindViewById<TextView>(Resource.Id.lblDolar).Text = Intent.GetStringExtra("USD");
				FindViewById<TextView>(Resource.Id.lblPeso).Text = Intent.GetStringExtra("CLP");
				FindViewById<Button>(Resource.Id.btnSalir).Click += (sender, e) => {

					Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
				};

			}
			catch (Exception ex)
			{
				Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
			}


		}
	}
}
