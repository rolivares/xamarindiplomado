﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace lab03
{
	[Activity(Label = "Conversor Moneda", MainLauncher = true)]
	public class MainActivity : Activity
	{

		protected override void OnCreate(Bundle savedInstanceState)
		{

			base.OnCreate(savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.ListViewLayout);

			var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			path = Path.Combine(path, "Carioca.db3");

			var conn = new SQLite.SQLiteConnection(path);


			//conn.DropTable<Player>();
			//conn.DropTable<Game>();

			conn.CreateTable<Player>();
			conn.CreateTable<Game>();



			//var game = new Game() { };
			//conn.Insert(game);
			//var players = Player.CreatePlayers(20, game);
			//conn.InsertAll(players);

			IList<Player> players = conn.Table<Player>().ToList();
			if (players.Count == 0)
			{
				var game = new Game() { };
				conn.Insert(game);
				players = Player.CreatePlayers(20, game);
				conn.InsertAll(players);
			}


			var lv = FindViewById<ListView>(Resource.Id.listView1);
			var adapter = new PlayersAdapter(players);

			lv.Adapter = adapter;

			//var btn = FindViewById<Button>(Resource.Id.btnConvertir);
			//btn.Click += (sender, e) =>
			//{
			//	try
			//	{
			//		var pesos = FindViewById<EditText>(Resource.Id.txtpesos).Text;
			//		var dolares = FindViewById<EditText>(Resource.Id.txtdolares).Text;
			//		var intent = new Intent(this, typeof(ActivitySecundaria));
			//		intent.PutExtra("USD", dolares);
			//		intent.PutExtra("CLP", pesos);
			//		StartActivity(intent);
			//	}
			//	catch (System.Exception ex)
			//	{
			//		Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
			//	}


			//};
		}
	}
}

