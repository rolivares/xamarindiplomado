﻿using System;
using System.Collections;
using System.Collections.Generic;
using Android.Views;
using Android.Widget;

namespace lab03
{
	public class PlayersAdapter : BaseAdapter<Player>
	{
		private IList<Player> _players;

		public PlayersAdapter(IList<Player> players)
		{
			_players = players;
		}

		public override Player this[int position]
		{
			get
			{
				return _players[position];
			}
		}

		public override int Count
		{
			get
			{
				return _players.Count;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var inflater = LayoutInflater.From(parent.Context);
			var playerView = inflater.Inflate(Resource.Layout.PlayerItemView, parent, false);

			playerView.FindViewById<TextView>(Resource.Id.txtName).Text = _players[position].Name;
			playerView.FindViewById<TextView>(Resource.Id.txtScore).Text = _players[position].Score.ToString();

			return playerView;
		}
	}
}
