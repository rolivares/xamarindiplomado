﻿using System;
using System.Collections.Generic;
using SQLite;

namespace lab03
{
	public class Game
	{
		public Game()
		{
		}

		[PrimaryKey, AutoIncrement]
		public int Id
		{
			get;
			set;
		}

		[MaxLength(30)]
		public string Title
		{
			get;
			set;
		}

		//public virtual List<Player> Players
		//{
		//	get;
		//	set;
		//}

	}
}
