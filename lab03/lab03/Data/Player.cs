﻿using System;
using System.Collections.Generic;
using SQLite;

namespace lab03
{
	public class Player
	{
		public Player()
		{
		}

		[PrimaryKey, AutoIncrement]
		public int Id
		{
			get;
			set;
		}

		[MaxLength(50)]
		public string Name
		{
			get;
			set;
		}

		public int Score
		{
			get;
			set;
		}


		[Indexed]
		public int GameId
		{
			get;
			set;
		}

		public static IList<Player> CreatePlayers(int count, Game game)
		{
			var players = new List<Player>();
			var rnd = new Random(DateTime.Now.Second);
			for (int i = 0; i < count; i++)
			{
				players.Add(new Player() { 
					Name = Guid.NewGuid().ToString(), 
					Score = rnd.Next(0, 200),
					GameId = game.Id
				});
			}

			return players;
		}

		public override string ToString()
		{
			return this.Name;
		}
	}
}
