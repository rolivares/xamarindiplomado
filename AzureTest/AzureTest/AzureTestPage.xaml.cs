﻿using AzureTest.ViewModels;
using Xamarin.Forms;

namespace AzureTest
{
	public partial class AzureTestPage : ContentPage
	{
		private PlayersViewModel viewModel;
		public AzureTestPage()
		{
			this.Padding = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 0);

			InitializeComponent();
			viewModel = new PlayersViewModel();
			this.BindingContext = viewModel;
			viewModel.LoadPlayers();
		}
	}
}
