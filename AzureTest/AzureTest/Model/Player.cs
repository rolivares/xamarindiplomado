﻿using System;
using Microsoft.WindowsAzure.MobileServices;

namespace AzureTest.Model
{
	public class Player : BaseObservableObject
	{
		private string _id;
		public string Id
		{
			get { return _id; }
			set { _id = value; OnPropertyChanged(); }
		}

		private DateTime _createdAt;
		public DateTime CreatedAt
		{
			get { return _createdAt; }
			set { _createdAt = value; OnPropertyChanged(); }
		}

		private DateTime _updatedAt;
		public DateTime UpdatedAt
		{
			get { return _updatedAt; }
			set { _updatedAt = value; OnPropertyChanged(); }
		}

		private string _version;
		[Version]
		public string Version
		{
			get { return _version; }
			set { _version = value; OnPropertyChanged(); }
		}

		private bool _deleted;
		public bool Deleted
		{
			get { return _deleted; }
			set { _deleted = value; OnPropertyChanged(); }
		}

		private int _currentScore;
		public int CurrentScore
		{
			get { return _currentScore; }
			set { _currentScore = value; OnPropertyChanged(); }
		}

		private string _name;
		public string Name
		{
			get { return _name; }
			set { _name = value; OnPropertyChanged(); }
		}
	}
}
