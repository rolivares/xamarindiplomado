﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzureTest.Model
{
	public class BaseObservableObject : INotifyPropertyChanged
	{
		public BaseObservableObject()
		{
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate
		{

		};

		public void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));

		}
	}
}
