﻿using AzureTest.Services;
using Xamarin.Forms;

namespace AzureTest
{
	public partial class App : Application
	{
		private AzureTest.AzureTestPage _page;
		public App()
		{		
			_page = new AzureTestPage();
			InitializeComponent();
			MainPage = _page;
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
