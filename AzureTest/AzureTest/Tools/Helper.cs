﻿using System;
using Newtonsoft.Json;

namespace AzureTest
{
	public static class JsonHelpers
	{
		public static string Serialize(object value)
		{
			var ser = JsonConvert.SerializeObject(value);
			return ser;
		}

		public static T Deserialize<T>(string value)
		{
			return (T)JsonConvert.DeserializeObject<T>(value);
		}



	}
}
