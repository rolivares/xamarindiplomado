﻿using System;
using System.Collections.ObjectModel;
using AzureTest.Model;
using AzureTest.Services;
using Xamarin.Forms;

namespace AzureTest.ViewModels
{
	public class PlayersViewModel : BaseObservableObject
	{
		public ObservableCollection<Player> Players { get; set; }
		public Command LoadPlayersCommand { get; private set; }

		private bool _isRunning;
		public bool IsBusy
		{
			get { return _isRunning; }
			set { _isRunning = value; OnPropertyChanged(); }
		}

		public PlayersViewModel()
		{
			this.Players = new ObservableCollection<Player>();
			this.LoadPlayersCommand = new Command(LoadPlayers);
		}

		public async void LoadPlayers()
		{
			this.IsBusy = true;
			var service = new PlayerAzureClient();
			//var service = new PlayersService();
			var players = await service.GetAll();
			if (players != null)
			{
				this.Players.Clear();
				foreach (var item in players)
				{
					this.Players.Add(item);
				}
			}
			this.IsBusy = false;
		}


	}
}
