﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;


namespace AzureTest.Services
{
	public class BaseRestService<T>
	{
		private const string _serviceUrl = "http://carioca-scorer.azurewebsites.net/Tables";

		private static void PrepareRequest(HttpClient httpClient)
		{
			httpClient.DefaultRequestHeaders.Accept.Clear();
			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			httpClient.DefaultRequestHeaders.Add("ZUMO-API-VERSION", "2.0.0");
		}

		private static async Task<IEnumerable<T>> ProcessAsList(HttpResponseMessage responseAsync)
		{
			var result = Enumerable.Empty<T>();

			if (responseAsync.IsSuccessStatusCode)
			{
				var json = await responseAsync.Content.ReadAsStringAsync().ConfigureAwait(false);

				result = await Task.Run(() =>
				{
					return JsonConvert.DeserializeObject<IEnumerable<T>>(json);
				}).ConfigureAwait(false);
			}

			return result;
		}

		public async Task<IEnumerable<T>> Save(IEnumerable<T> elements)
		{
			var result = Enumerable.Empty<T>();
			using (var httpClient = new HttpClient())
			{
				httpClient.DefaultRequestHeaders.Accept.Clear();
				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				httpClient.DefaultRequestHeaders.Add("ZUMO-API-VERSION", "2.0.0");

				HttpContent content = null;

				var responseAsync = await httpClient.PutAsync(new Uri(_serviceUrl + "/" + typeof(T).Name), content).ConfigureAwait(false);

				result = await ProcessAsList(responseAsync);
			}

			return result;
		}

		public async Task<IEnumerable<T>> GetAll()
		{
			IEnumerable<T> result;
			using (var httpClient = new HttpClient())
			{
				PrepareRequest(httpClient);

				var responseAsync = await httpClient.GetAsync(new Uri(_serviceUrl + "/" + typeof(T).Name)).ConfigureAwait(false);
				result = await ProcessAsList(responseAsync);
			}

			return result;
		}


	}
}
