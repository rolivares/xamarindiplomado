﻿using System;
using System.Collections.Generic;
using AzureTest.Model;

namespace AzureTest.Services
{
	public static class DummyObjectsCreator
	{

		public static IList<Player> CreatePlayers(int q)
		{
			var rnd = new Random(DateTime.Now.Second);
			var list = new List<Player>();
			for (int i = 0; i < q; i++)
			{
				list.Add(new Player()
				{
					Name = Guid.NewGuid().ToString().Remove('-'),
					CurrentScore = rnd.Next(0, 100)
				});
			};

			return list;
		}
	}
}
