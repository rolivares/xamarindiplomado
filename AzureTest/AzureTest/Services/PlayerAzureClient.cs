﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AzureTest.Model;
using Microsoft.WindowsAzure.MobileServices;

namespace AzureTest
{
	

	public class PlayerAzureClient
	{
		private const string _serviceUrl = "http://carioca-scorer.azurewebsites.net";
		private IMobileServiceClient _client;
		private IMobileServiceTable<Player> _table;

		public PlayerAzureClient()
		{
			
			_client = new MobileServiceClient(_serviceUrl);
			_table = _client.GetTable<Player>();
		}


		public async Task<IEnumerable<Player>> GetAll()
		{

			var result =  await _table.ToEnumerableAsync();

			return result;
			//IEnumerable<Player> list = await Task.Run(() => new List<Player>());
			//return list;
		

		}
	}
}
