﻿using System;

using Xamarin.Forms;

namespace Greetings
{
	public class GreetingsPage : ContentPage
	{
		public GreetingsPage()
		{
			var myLabel = new Label() { Text = "Greetings Xamarins Forms !!!" };
			this.Content = myLabel;
			Padding = new Thickness(0, 20, 0 ,0);
		}
	}
}

