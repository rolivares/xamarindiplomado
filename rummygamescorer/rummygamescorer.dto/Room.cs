﻿using System;
using System.Collections.Generic;

namespace rummygamescorer.dto
{
	public class Room
	{
		public string Title { get; set; }

		public IList<Game> Games { get; set; }
		public IList<Player> Players { get; set; }

	}
}
