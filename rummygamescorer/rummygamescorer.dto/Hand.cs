﻿using System;
using System.Collections.Generic;

namespace rummygamescorer.dto
{
	public class Hand
	{
		public int Total { get; set; }
		public string Title { get; set; }
	}
}
