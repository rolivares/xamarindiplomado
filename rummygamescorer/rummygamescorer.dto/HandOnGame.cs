﻿using System;
using System.Collections.Generic;

namespace rummygamescorer.dto
{
	public class HandOnGame :  Hand
	{
		
		public int PlayOrder { get; set; }
		public IList<PlayerScore> Scores { get; set; }

		public HandOnGame()
		{
		}


		public static IList<Hand> GetCommonHands()
		{
			var hands = new List<Hand>();
			hands.Add(new Hand() { Total = 6, Title = "2T" });
			hands.Add(new Hand() { Total = 7, Title = "1T 1E" });
			hands.Add(new Hand() { Total = 8, Title = "2E" });
			hands.Add(new Hand() { Total = 9, Title = "3T" });
			hands.Add(new Hand() { Total = 10, Title = "2T 1E" });
			hands.Add(new Hand() { Total = 11, Title = "1T 2E" });

			hands.Add(new Hand() { Total = 12, Title = "4T" });
			hands.Add(new Hand() { Total = 12, Title = "3E" });

			hands.Add(new Hand() { Total = 13, Title = "Escala loca" });
			hands.Add(new Hand() { Total = 13, Title = "Escala sucia" });
			hands.Add(new Hand() { Total = 13, Title = "Escala Real" });

			return hands;
		}
	}
}
