﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace rummygamescorer.dto
{
	public enum GameStatus
	{
		New,
		Started,
		Stopped,
		Ended
	}



	/// <summary>
	/// Current game that have a score associated
	/// </summary>
	public class Game
	{

		public DateTime CreatedOn { get; set; }
		public DateTime FinishedOn { get; set; }

		/// <summary>
		/// Set of hands to play on the game
		/// </summary>
		public IList<HandOnGame> Hands { get; set; }


		public Game()
		{
		}

		public IList<PlayerScore> GetLeaderBoard()
		{
			var leaderBoard = Hands.SelectMany(x => x.Scores)
								   .GroupBy(x => x.PlayerId)
								   .Select(g => new PlayerScore() { PlayerId = g.Key, Score = g.Sum(s => s.Score) })
								   .ToList();

			return leaderBoard;
		}
	}
}
