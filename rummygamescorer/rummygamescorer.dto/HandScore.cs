﻿using System;
namespace rummygamescorer.dto
{
	public class HandScore
	{
		public int PlayerId { get; set; }
		public int? Score { get; set; }


	}
}
