﻿using System;
namespace rummygamescorer.dto
{
	public class PlayerScore
	{
		public int PlayerId { get; set; }
		public Player Player { get; set; }
		public int Score { get; set; }


	}
}
